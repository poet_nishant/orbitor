package iammert.com.androidarchitecture.data;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import iammert.com.androidarchitecture.data.local.dao.PoemDao;
import iammert.com.androidarchitecture.data.local.entity.MovieEntity;
import iammert.com.androidarchitecture.data.local.entity.PoemEntity;
import iammert.com.androidarchitecture.data.remote.PoemDBService;
import iammert.com.androidarchitecture.data.remote.model.MoviesResponse;
import iammert.com.androidarchitecture.data.remote.model.PoemResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by nishant.singh on 16-01-2018.
 */

public class PoemRepository {

    private final PoemDao poemDao;
    private final PoemDBService poemDBService;
    private List<PoemEntity> result;
    Resource<List<PoemEntity>> resource;
    LiveData<Resource<List<PoemEntity>>> r;

    @Inject
    public PoemRepository(PoemDao poemDao, PoemDBService poemDBService) {
        this.poemDao = poemDao;
        this.poemDBService = poemDBService;
    }


    public LiveData<Resource<List<PoemEntity>>> loadPopularPoem() {
//        Log.d("test", "Inside methid loadPopularPoem");
//        Call<List<PoemEntity>> call = poemDBService.loadPoem();
//        MutableLiveData<Resource<List<PoemEntity>>> poemList = new MutableLiveData<Resource<List<PoemEntity>>>();
//        // network call
//        call.enqueue(new Callback<List<PoemEntity>>() {
//            @Override
//            public void onResponse(Call<List<PoemEntity>> call, Response<List<PoemEntity>> response) {
//                if (response.body() != null) {
//                    Log.d("test", "User ID: " + response.body().get(0).getDescription());
//                    insertIntoDataBase(response.body());
//                    List<PoemEntity> p=new ArrayList<PoemEntity>();
//                    for(int i=0;i<response.body().size();i++){
//                     p.add(response.body().get(i));
//                    }
//                    Resource<List<PoemEntity>> r=new Resource<>("", p,"");
//                    poemList.setValue(r);
//                } else {
//                    Log.d("test", "poem not fetched");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<List<PoemEntity>> call, Throwable t) {
//                Log.e("test", t.toString());
//            }
//        });

//        return new NetworkBoundResource<List<PoemEntity>, PoemResponse>() {
//
//            @Override
//            protected void saveCallResult(@NonNull PoemResponse item) {
//                // Saving to database
//                poemDao.savePoem(item.getResults());
//            }
//
//            @NonNull
//            @Override
//            protected LiveData<List<PoemEntity>> loadFromDb() {
//                return poemDao.loadPoem();
//            }
//
//            @NonNull
//            @Override
//            protected Call<List<PoemEntity>> createCall() {
//
//                return poemDBService.loadPoem();
//            }
//        }.getAsLiveData();

//        return null;


        return new NetworkBoundResource<List<PoemEntity>, PoemResponse>() {

            @Override
            protected void saveCallResult(@NonNull PoemResponse item) {
                // Saving to database
                poemDao.savePoem(item.getResults());
            }

            @NonNull
            @Override
            protected LiveData<List<PoemEntity>> loadFromDb() {
                return poemDao.loadPoem();
            }

            @NonNull
            @Override
            protected Call<PoemResponse> createCall() {

                return poemDBService.loadPoem();
            }
        }.getAsLiveData();


    }

    public void insertIntoDataBase(List<PoemEntity> list) {
        new Thread(new Runnable() {
            @Override
            public void run() {
//                ArrayList<PoemEntity> list=new ArrayList<>();
//                PoemEntity pe=new PoemEntity();
//                pe.setId(1);
//                pe.setPoem_date("17-01-18");
//                pe.setPoet_name("Sapna");
//                pe.setTitle("Dream");
//                pe.setDescription("fvsfjhbfjhbsfjhsbfjhb");
//                list.add(pe);
                poemDao.savePoem(list);
            }
        }).start();
    }
//    public LiveData<Po> getMovie(int id){
//        return poemDao.getMovie(id);
//    }
}
