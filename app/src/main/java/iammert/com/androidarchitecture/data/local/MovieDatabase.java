package iammert.com.androidarchitecture.data.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import iammert.com.androidarchitecture.data.local.dao.MovieDao;
import iammert.com.androidarchitecture.data.local.dao.PoemDao;
import iammert.com.androidarchitecture.data.local.entity.MovieEntity;
import iammert.com.androidarchitecture.data.local.entity.PoemEntity;

/**
 * Created by mertsimsek on 19/05/2017.
 */
@Database(entities = {MovieEntity.class, PoemEntity.class}, version = 3)
public abstract class MovieDatabase extends RoomDatabase{

    public abstract MovieDao movieDao();
    public abstract PoemDao poemDao();
}
