package iammert.com.androidarchitecture.data.local.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import iammert.com.androidarchitecture.data.local.entity.MovieEntity;
import iammert.com.androidarchitecture.data.local.entity.PoemEntity;

/**
 * Created by nishant.singh on 11-01-2018.
 */
@Dao
public interface PoemDao {

    @Query("SELECT * FROM poem")
    LiveData<List<PoemEntity>> loadPoem();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void savePoem(List<PoemEntity> poemEntities);

//    @Query("SELECT * FROM movies WHERE id=:id")
//    LiveData<MovieEntity> getMovie(int id);
}
