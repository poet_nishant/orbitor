package iammert.com.androidarchitecture.data.local.entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nishant.singh on 11-01-2018.
 */
@Entity(tableName = "poem")
public class PoemEntity {

    @PrimaryKey
    @ColumnInfo(name = "title")
    @SerializedName("Title")
    @Expose
    private String title;

    @ColumnInfo(name = "author")
    @SerializedName("Auther_Name")
    @Expose
    private String autherName;

    @ColumnInfo(name = "description")
    @SerializedName("Description")
    @Expose
    private String description;


    @ColumnInfo(name = "poem_date")
    @SerializedName("Date")
    @Expose
    private String date;

    public String getAutherName() {
        return autherName;
    }

    public void setAutherName(String autherName) {
        this.autherName = autherName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description.trim();
    }

    public void setDescription(String description) {
        this.description = description.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
