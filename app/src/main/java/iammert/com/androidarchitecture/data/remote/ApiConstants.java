package iammert.com.androidarchitecture.data.remote;

/**
 * Created by mertsimsek on 19/05/2017.
 */

public class ApiConstants {

    public static final String ENDPOINT = "https://api.themoviedb.org/3/";
    public static final String IMAGE_ENDPOINT_PREFIX = "https://image.tmdb.org/t/p/w500/";
    public static final String API_KEY = "75efd4902e880c1abec26a7ad84a9530";
    public static final int TIMEOUT_IN_SEC = 15;

    //Poem
    public static final String POEMENDPOINT = "https://a3b0d136.ngrok.io";
}
