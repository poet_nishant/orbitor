package iammert.com.androidarchitecture.data.remote;

import java.util.List;

import iammert.com.androidarchitecture.data.local.entity.PoemEntity;
import iammert.com.androidarchitecture.data.remote.model.MoviesResponse;
import iammert.com.androidarchitecture.data.remote.model.PoemResponse;
import iammert.com.androidarchitecture.ui.chatbot.ChatMessage;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by nishant.singh on 11-01-2018.
 */

public interface PoemDBService {
//    @GET("/getPoem")
//    Call<List<PoemEntity>> loadPoem();

    @GET("/getPoem")
    Call<PoemResponse> loadPoem();

    @POST("/chatBot")
    Call<ChatMessage> chat();

}
