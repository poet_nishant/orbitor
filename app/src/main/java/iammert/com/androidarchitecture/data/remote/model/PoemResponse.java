package iammert.com.androidarchitecture.data.remote.model;

import java.util.List;

import iammert.com.androidarchitecture.data.local.entity.PoemEntity;

/**
 * Created by nishant.singh on 11-01-2018.
 */

public class PoemResponse {
    private List<PoemEntity> results;

    public List<PoemEntity> getResults() {
        return results;
    }

    public void setResults(List<PoemEntity> results) {
        this.results = results;
    }
}
