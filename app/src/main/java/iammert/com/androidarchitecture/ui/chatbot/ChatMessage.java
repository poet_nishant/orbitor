package iammert.com.androidarchitecture.ui.chatbot;


import android.arch.lifecycle.ViewModel;

import javax.inject.Inject;

import iammert.com.androidarchitecture.data.MovieRepository;

/**
 * Created by beast on 14/4/17.
 */

public class ChatMessage extends ViewModel {

    private String msgText;
    private String msgUser;

    @Inject
    public ChatMessage() {
    }

    public ChatMessage(String msgText, String msgUser) {
        this.msgText = msgText;
        this.msgUser = msgUser;

    }


//    public ChatMessage() {
//
//    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public String getMsgUser() {
        return msgUser;
    }

    public void setMsgUser(String msgUser) {
        this.msgUser = msgUser;
    }
}
