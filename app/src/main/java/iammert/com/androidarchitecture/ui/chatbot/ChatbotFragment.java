package iammert.com.androidarchitecture.ui.chatbot;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;
import iammert.com.androidarchitecture.R;
import iammert.com.androidarchitecture.data.remote.PoemDBService;
import iammert.com.androidarchitecture.databinding.FragmentChatbotBinding;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import com.firebase.ui.database.FirebaseRecyclerAdapter;

public class ChatbotFragment extends Fragment {


    @Inject
    PoemDBService poemDBService;

    FragmentChatbotBinding dataBinding;
    RecyclerView recyclerView;
    EditText editText;
    RelativeLayout addBtn;
    DatabaseReference ref;
    FirebaseRecyclerAdapter<ChatMessage, chat_rec> adapter;
    Boolean flagFab = true;

    //private AIService aiService;

    public ChatbotFragment() {
        // Required empty public constructor
    }


    public static ChatbotFragment newInstance() {
        ChatbotFragment fragment = new ChatbotFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidSupportInjection.inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreateView(inflater, container, savedInstanceState);
        dataBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_chatbot, container, false);
        dataBinding.recyclerView.setHasFixedSize(true);
        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        dataBinding.recyclerView.setLayoutManager(linearLayoutManager);

        ref = FirebaseDatabase.getInstance().getReference();
        ref.keepSynced(true);

        dataBinding.addBtn.setOnClickListener(v -> asyncTask());


        registerListener(dataBinding, linearLayoutManager);


        // dataBinding.recyclerView.setAdapter(new MovieListAdapter());
        return dataBinding.getRoot();

    }

    private void registerListener(FragmentChatbotBinding dataBinding, LinearLayoutManager linearLayoutManager) {


        adapter = new FirebaseRecyclerAdapter<ChatMessage, chat_rec>(ChatMessage.class, R.layout.msglist, chat_rec.class, ref.child("chat")) {
            @Override
            protected void populateViewHolder(chat_rec viewHolder, ChatMessage model, int position) {

                if (null != model.getMsgUser() && model.getMsgUser().equals("user")) {


                    viewHolder.rightText.setText(model.getMsgText());

                    viewHolder.rightText.setVisibility(View.VISIBLE);
                    viewHolder.leftText.setVisibility(View.GONE);
                } else {
                    viewHolder.leftText.setText(model.getMsgText());

                    viewHolder.rightText.setVisibility(View.GONE);
                    viewHolder.leftText.setVisibility(View.VISIBLE);
                }
            }
        };


        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);

                int msgCount = adapter.getItemCount();
                int lastVisiblePosition = linearLayoutManager.findLastCompletelyVisibleItemPosition();

                if (lastVisiblePosition == -1 ||
                        (positionStart >= (msgCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    dataBinding.recyclerView.scrollToPosition(positionStart);

                }

            }
        });

        dataBinding.recyclerView.setAdapter(adapter);

    }

    private void asyncTask() {

        String message = dataBinding.editText.getText().toString().trim();

        if (!message.equals("")) {

            ChatMessage chatMessage = new ChatMessage(message, "user");
            ref.child("chat").push().setValue(chatMessage);

            Call<ChatMessage> call = poemDBService.chat();
            call.enqueue(new Callback<ChatMessage>() {
                @Override
                public void onResponse(Call<ChatMessage> call, Response<ChatMessage> response) {
                    Log.d("test", "post Success" + response.body().getMsgUser());
                    Log.d("test", "post Success" + response.body().getMsgText());


                    ChatMessage chatMessage = new ChatMessage(response.body().getMsgText(), response.body().getMsgUser());
                    ref.child("chat").push().setValue(chatMessage);

                }

                @Override
                public void onFailure(Call<ChatMessage> call, Throwable t) {
                    Log.e("test", "error in post to python");
                }
            });

        }

        dataBinding.editText.setText("");

    }


}
