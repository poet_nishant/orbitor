package iammert.com.androidarchitecture.ui.main;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import iammert.com.androidarchitecture.ui.chatbot.ChatbotFragment;

/**
 * Created by mertsimsek on 20/05/2017.
 */

public class MoviesPagerAdapter extends FragmentStatePagerAdapter {

    private static final String[] titles = new String[]{"Poem", "chat", "Devops"};

    public MoviesPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {

        String title = (String) getPageTitle(i);
        if (title.equalsIgnoreCase("Popular")) {
            return PoemListFragment.newInstance();
        } else if (title.equalsIgnoreCase("chat")) {
            return ChatbotFragment.newInstance();
        } else {
            return PoemListFragment.newInstance();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}
