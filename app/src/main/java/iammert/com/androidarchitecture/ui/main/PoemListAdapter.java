package iammert.com.androidarchitecture.ui.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import iammert.com.androidarchitecture.data.local.entity.PoemEntity;
import iammert.com.androidarchitecture.databinding.ItemMovieListBinding;
import iammert.com.androidarchitecture.databinding.ItemPoemListBinding;
import iammert.com.androidarchitecture.ui.BaseAdapter;

/**
 * Created by nishant.singh on 24-02-2018.
 */

public class PoemListAdapter extends BaseAdapter<PoemListAdapter.PoemViewHolder, PoemEntity> {

    private List<PoemEntity> poemEntities;

    //private final MovieListCallback movieListCallback;

    public PoemListAdapter() {
        poemEntities = new ArrayList<>();
        //this.movieListCallback = movieListCallback;
    }

    @Override
    public void setData(List<PoemEntity> poemEntities) {
        this.poemEntities = poemEntities;
        notifyDataSetChanged();
    }

    @Override
    public PoemListAdapter.PoemViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return PoemListAdapter.PoemViewHolder.create(LayoutInflater.from(viewGroup.getContext()), viewGroup);
    }

    @Override
    public void onBindViewHolder(PoemListAdapter.PoemViewHolder viewHolder, int i) {
        viewHolder.onBind(poemEntities.get(i));
    }

    @Override
    public int getItemCount() {
        return poemEntities.size();
    }

    static class PoemViewHolder extends RecyclerView.ViewHolder {

        public static PoemListAdapter.PoemViewHolder create(LayoutInflater inflater, ViewGroup parent) {
            ItemPoemListBinding itemPoemListBinding = ItemPoemListBinding.inflate(inflater, parent, false);
            return new PoemListAdapter.PoemViewHolder(itemPoemListBinding);
        }

        ItemPoemListBinding binding;

        public PoemViewHolder(ItemPoemListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void onBind(PoemEntity poemEntity) {
            binding.setPoem(poemEntity);
            binding.executePendingBindings();
        }
    }
}