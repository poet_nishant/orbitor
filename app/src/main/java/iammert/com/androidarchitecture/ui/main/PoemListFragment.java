package iammert.com.androidarchitecture.ui.main;

import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import iammert.com.androidarchitecture.R;
import iammert.com.androidarchitecture.data.Resource;
import iammert.com.androidarchitecture.data.local.entity.PoemEntity;
import iammert.com.androidarchitecture.databinding.FragmentPoemListBinding;
import iammert.com.androidarchitecture.ui.BaseFragment;

public class PoemListFragment extends BaseFragment<PoemViewModel, FragmentPoemListBinding> implements LifecycleRegistryOwner {

    public PoemListFragment() {
        // Required empty public constructor
    }


    public static PoemListFragment newInstance() {
        Bundle args = new Bundle();
        PoemListFragment fragment = new PoemListFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Class getViewModel() {
        return PoemViewModel.class;
    }

    @Override
    public int getLayoutRes() {
        return R.layout.fragment_poem_list;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        dataBinding.poemRecyleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        dataBinding.poemRecyleView.setAdapter(new PoemListAdapter());
        return dataBinding.getRoot();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel.getPopularPoem()
                .observe(this, listResource -> dataBinding.setPoem(listResource));

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
