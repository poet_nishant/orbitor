package iammert.com.androidarchitecture.ui.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

import javax.inject.Inject;

import iammert.com.androidarchitecture.data.PoemRepository;
import iammert.com.androidarchitecture.data.Resource;
import iammert.com.androidarchitecture.data.local.entity.PoemEntity;

/**
 * Created by nishant.singh on 15-01-2018.
 */

public class PoemViewModel extends ViewModel {

    private final LiveData<Resource<List<PoemEntity>>> poemList;

    @Inject
    public PoemViewModel(PoemRepository poemRepository) {
        poemList = poemRepository.loadPopularPoem();
    }


    LiveData<Resource<List<PoemEntity>>> getPopularPoem() {
        return poemList;
    }
}

